import os
import boto3

def setup_ip(current_ip):

    # Create EC2 client to access security groups
    ec2 = boto3.client('ec2')

    # Check security group to see if this IP already has a rule
    groups = ec2.describe_security_groups(GroupIds=[os.environ.get("SECURITY_GROUP")])
    rules = groups['SecurityGroups'][0]['IpPermissions']

    # Verify that the current IPv4 address is already in the security group rules
    for rule in rules:
        for ip in rule['IpRanges']:
            if current_ip in ip['CidrIp']:
                print("IP Address already in Security Group, not adding.")
                return

    # Update ingress rules
    ec2.authorize_security_group_ingress(
        GroupId=os.environ.get("SECURITY_GROUP"),
        CidrIp=current_ip+"/32",
        IpProtocol="TCP",
        FromPort=3306,
        ToPort=3306
    )

    return