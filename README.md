# Description

Demonstrates a method of accessing a resource behing a VPC on Amazon that is 
utilizing a VPC with IP whitelisting. To use this with your own MySQL databse
hosted on AWS RDS you will need to make the DB publically available. This does 
not grant access to the DB from everywhere, but allows access to the DB from 
outside the VPC. The `setup_ip` function will be invoked to add the IP address 
to the inbound rules for the VPC at runtime and the rule will then be deleted by 
`revoke_ip` after a succesful connection to the DB. 

Run `main.py` to test.

# Disclaimer about Data Races

If multiple instances of this function are run in parallel, there is a chance of 
a data race occuring in which one instance revokes the IP address of another. If
this is going to occur, the `revoke_ip` functionality needs to be removed and 
managed by another service that can safely remove the unneeded inbound rules at
a time when there are no instances running.

# Environment Variables
* AWS_ACCESS_KEY_ID
* AWS_SECRET_ACCESS_KEY
* AWS_DEFAULT_REGION
* SECURITY_GROUP
* DB_ENDPOINT
* DB_USER
* DB_PASSWORD
* DATABASE

These can be specified in a .env file which will be loaded in during runtime.