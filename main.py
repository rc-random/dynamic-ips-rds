import os
import boto3
import pymysql
import requests
from dotenv import load_dotenv
load_dotenv()

from setup_ip import setup_ip
from revoke_ip import revoke_ip

if __name__ == "__main__":

    # Fetch current IP
    res = requests.get("https://checkip.amazonaws.com")
    res.raise_for_status()
    current_ip = res.content.decode().strip('\n')

    # Add IP to Security Group
    setup_ip(current_ip)

    # Connect to Database
    conn = pymysql.connect(
        host=os.environ.get("DB_ENDPOINT"),
        user=os.environ.get("DB_USER"),
        password=os.environ.get("DB_PASSWORD"),
        database=os.environ.get("DATABASE")
    )

    # Print version of Database to verify succseful connection
    with conn:
        cur = conn.cursor()
        cur.execute("SELECT VERSION()")
        version = cur.fetchone()
        print("Database version: {} ".format(version[0]))

    # Remove from Security Group
    # NOTE: This should not be used if multiple jobs are being processed in 
    # parallel! This will lead to a race condition and will cause jobs to fail
    # because they are no longer allowed to access the DB. If multiple jobs are 
    # going to be run in parallel, then a separate cron job should be setup to 
    # run on an interval during downtime (i.e. 2am?). The cron job will then go 
    # in and clear old inbound rules to keep access the the DB limited.
    revoke_ip(current_ip)