import os
import boto3

def revoke_ip(current_ip):

    # Create EC2 client to access security groups
    ec2 = boto3.client('ec2')

    # Check security group to see if this IP already has a rule
    groups = ec2.describe_security_groups(GroupIds=[os.environ.get("SECURITY_GROUP")])
    rules = groups['SecurityGroups'][0]['IpPermissions']

    # Verify that the current IPv4 address is already in the security group rules
    inbound_rule = None
    for rule in rules:
        for ip in rule['IpRanges']:
            if current_ip in ip['CidrIp']:
                inbound_rule = rule
                break
    
    if inbound_rule is None:
        print("Unable to find inbound rule for the current IP address.")
        return

    # Revoke ingress rules
    ec2.revoke_security_group_ingress(
        GroupId=os.environ.get("SECURITY_GROUP"),
        IpPermissions=[inbound_rule]
    )

    return